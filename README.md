# 微信小程序商城（JAVA）

#### Open-Shop项目介绍
Open-Shop是一个代码全部开源的微信小程序商城。本程序是借鉴码云其他开源项目。把所有坑都已经躺过了，可以直接使用。如需帮助请加qq群：877611106。
官方网站：http://www.51app.ink/ 微信小程序后台地址：https://shop.51shop.ink/login.html

## Open-Shop页面展示
### 小程序演示效果
![](http://pgf1db9j6.bkt.clouddn.com/6.jpg "前段演示")
### Open-Shop演示地址
![](http://pgf1db9j6.bkt.clouddn.com/Advertisement4.jpg "演示地址")
